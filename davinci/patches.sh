#!/bin/bash

set -e
source build/envsetup.sh

changes=(
  278020 # FODCircleView: handle more visibility cases
  278021 # FODCircleView: let's smoothly fade-out the view, before going to View.GONE
  272303 # DozeSensors: only use proximity sensor if supported
  266130 # FODCircleView: Add Support for custom FP pressed icon
  274433 # SystemUI: use DOUBLE_TAP_TO_WAKE setting also for wake from aod
  280082 # FingerprintService: hide InDisplayFingerprintView on Error
  276156 # sdm: mark FOD pressed layer by setting a bit on ZPOS
  279890 # Allow to drop existing fd cache of cgroup path
)
repopick ${changes[@]}&

repopick -p 278063 # hal: Add tfa98xx feedback extension
repopick -p 277176 # VolumeDialog: Implement expand/collapse animation
repopick -p 279893 # [DNM] Enable USAP by default
repopick -p 275821 # interfaces: Fix blueprint generation
repopick -p 272451 # org.ifaa.android.manager: Add ifaa apis to unrestricted greylist
repopick -p 277273 # MediaProvider: Check hidden directories recursively
repopick -t ten-network-isolation
repopick -t ten-firewall