#!/bin/bash

# install deps
apt-get -qq update
apt-get -qqy upgrade
apt-get install -y bc bison bsdmainutils build-essential ccache cgpt cron \
      curl flex g++-multilib gcc-multilib git gnupg gnutls-bin gperf imagemagick kmod \
      lib32ncurses5-dev lib32readline-dev lib32z1-dev libesd0-dev liblz4-tool \
      libncurses5-dev libsdl1.2-dev libssl-dev libwxgtk3.0-dev libxml2 \
      libxml2-utils lsof lzop maven openjdk-8-jdk openssl pngcrush procps python python3 rsync \
      schedtool squashfs-tools unzip wget xdelta3 xsltproc yasm zip zlib1g-dev

# download repo
mkdir -p bin/
curl https://storage.googleapis.com/git-repo-downloads/repo > ./bin/repo
chmod a+x ./bin/repo

# create workspace
mkdir -p lineage/
cd lineage

# init repo
if [ ! -d "./lineage/.repo" ]; then
      ./bin/repo init -u https://github.com/LineageOS/android.git -b lineage-17.1
fi

cd ..

# copy manifest
mkdir -p ./lineage/.repo/local_manifests
cp ./davinci/manifest.xml ./lineage/.repo/local_manifests/manifest.xml

# create signing keys
if [ ! -d "./keys" ]; then
      chmod +x ./utils/make_key.sh
      mkdir -p keys

      KEYHOLDER="/C=US/ST=California/L=Mountain View/O=Android/OU=Android/CN=Android/emailAddress=android@android.com"
      for c in releasekey platform shared media networkstack; do
            ./utils/make_key.sh "keys/$c" "$KEYHOLDER"
      done
fi

# prepare patch scripts
chmod +x ./davinci/patches.sh
chmod +x ./microg/patches.sh