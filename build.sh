#!/bin/bash

cd lineage

# prepare java
export ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx4G"
export JAVA_TOOL_OPTIONS="-Xmx6g"

# prepare ccache
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache
ccache -M 50G

# sync
../bin/repo sync

# prepare env
source build/envsetup.sh
breakfast davinci

# apply device patches
../davinci/patches.sh

# apply microg patches
../microg/patches.sh

# move keys for soong
mkdir -p user-keys
cp ../keys/** user-keys/

brunch davinci